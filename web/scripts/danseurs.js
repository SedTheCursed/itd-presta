$(function(){
	function gestionPas(homme, femme, gent, lady)
	{
		var self		= this;
		
		this.homme		= homme;
		this.femme		= femme;
		this.gent		= gent;
		this.lady		= lady;			
		
		this.danseur	= function ()
		{
			self.gent.prop("checked", true);
			self.lady.prop("disabled", true);
		}
		
		this.danseuse	= function ()
		{		
			self.lady.prop("checked", true);
			self.lady.prop("disabled", false);
		}
		
		this.action		= function () {
			(self.homme.prop("checked"))?self.danseur():"";
			
			self.homme.on("change", self.danseur);
			self.femme.on("change", self.danseuse);
		}		
	}
	
	var homme, femme, gent, lady;
	
	if ($("#sed_prestationsbundle_dancer_sex_0").length > 0) {
		homme = $("#sed_prestationsbundle_dancer_sex_0");
	} else {
		homme = $("#dancer_edit_sex_0");
	}
	
	if ($("#sed_prestationsbundle_dancer_sex_1").length > 0) {
		femme = $("#sed_prestationsbundle_dancer_sex_1");
	} else {
		femme = $("#dancer_edit_sex_1");
	}
	
	if ($("#sed_prestationsbundle_dancer_preference_0").length > 0) {
		gent = $("#sed_prestationsbundle_dancer_preference_0");
	} else {
		gent = $("#dancer_edit_preference_0");
	}
	
	if ($("#sed_prestationsbundle_dancer_preference_1").length > 0) {
		lady = $("#sed_prestationsbundle_dancer_preference_1");
	} else {
		lady = $("#dancer_edit_preference_1");
	}

	 var gender = new gestionPas(homme, femme, gent, lady);
	
	gender.action();
});