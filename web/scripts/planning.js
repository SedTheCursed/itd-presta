$(function(){
	function addDance (element) {
		var dance = $(element).attr("data_name");
		
		currentDances.push(dance);
		$("#currentDances").append("<li>" + dance + "</li>");
	}
	
	function removeDance (element) {
		var dance = $(element).attr("data_name");
		
		if($.inArray(dance, currentDances) >= 0) {
			currentDances.splice($.inArray(dance, currentDances),1);
			
			$("#currentDances").text("");
			
			$.each(currentDances, function(index, dance) {
				$("#currentDances").append("<li>" + dance + "</li>");
			});
		}
	}
	var totalDuration	= parseInt($("#totalTime").attr("value")),
		bookedDuration 	= 0,
		currentDances	= [];
	
	if (!isNaN(parseInt($("input[type=radio]:checked").attr("data_duration")))) {
		finalDuration 	= parseInt($("input[type=radio]:checked").attr("data_duration"));
	} else {
		finalDuration	= 0;
	}
	
	
	//Calcul du temps de prestation non alloué au chargement de la page
	$(".dance_checkbox:checked").each(function(){
		bookedDuration += parseInt($(this).attr("data_duration"));
		currentDances.push($(this).attr("data_name"));
	});
		
	var remainingTime = totalDuration-(finalDuration+bookedDuration);
	
	if (!isNaN(remainingTime)) {
		$("#remainingTime").text(remainingTime);
	}
	
	remainingTime = parseInt($("#remainingTime").text());

	
	//Affichage des danses selectionnées au chargement de la page;
	var currentFinal = $("input[type=radio]:checked").attr("data_name");
	
	(currentFinal != null)? currentDances.push(currentFinal) : "";
	
	$.each(currentDances, function(index, dance) {
		$("#currentDances").append("<li>" + dance + "</li>");
	});

	
	//Modifications lié au changement de final
	$("input[type=radio]").each(function(){
		$(this).on("change", function(){
			//Gestion du temps restant lorsqu'on change de final
			remainingTime += finalDuration;
			
			(remainingTime<= totalDuration)? remainingTime = remainingTime : remainingTime = totalDuration;
			
			finalDuration = parseInt($(this).attr("data_duration"));
			
			remainingTime -= finalDuration;
			
			$("#remainingTime").text(remainingTime);
			
			//Ajout de la danse à la liste des danses séléctionnées
			addDance(this);
			
			//Selection des danseurs du final et suppression du nom du précédent final
			$("input[type=radio]:not(:checked)").each(function(){
				$(this).next().children().last().children().children("input").prop("checked",false);
				removeDance(this);
			});
			
			$(this).next().children().last().children().children("input").prop("checked",true);
		});
	});
	
	
	//Modifications liée à une danse planifiée
	$(".planned>.dance_checkbox").on("change",function(){
		var duration = parseInt($(this).attr("data_duration"));
		
		if ($(this).prop("checked")) { //Ajout d'une danse
			//Gestion du temps
			remainingTime -= duration;
			
			//Ajout de la danse à la liste des danses séléctionnées
			addDance(this);
			
			//Gestion des danseurs : auto-selection des danseurs actifs
			$(this).next().children().last().children(".dancers").first().children().children("input").prop("checked", true);
			
		} else { //Suppression d'une danse
			//Gestion du temps
			remainingTime += duration;
			(remainingTime<= totalDuration)? remainingTime = remainingTime : remainingTime = totalDuration;
			
			//Suppression de la danse à la liste des danses séléctionnées
			removeDance(this);
			
			//Gestion des danseurs : deselection de tous les danseurs
			$(this).next().children().last().children(".dancers").children().children("input").prop("checked", false);
		}
		
		$("#remainingTime").text(remainingTime);
	});
	
	//Modifications liée à une danse non planifiée
	$(".other>.dance_checkbox").on("change",function(){
		var duration = parseInt($(this).attr("data_duration"));
		
		if ($(this).prop("checked")) { //Ajout d'une danse
			//Gestion du temps
			remainingTime -= duration;
			
			//Ajout de la danse à la liste des danses séléctionnées
			addDance(this);
		} else {//Suppression d'une danse
			//Gestion du temps
			remainingTime += duration;
			(remainingTime<= totalDuration)? remainingTime = remainingTime : remainingTime = totalDuration;
			
			//Suppression de la danse à la liste des danses séléctionnées
			removeDance(this);
			
			//Gestion des danseurs : deselection de tous les danseurs
			$(this).next().children().last().children().children("input").prop("checked", false);		
		}
		
		$("#remainingTime").text(remainingTime);
	});
});