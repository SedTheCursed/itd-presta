$(function(){
	function menuItems(width, originalText)
	{
		$("#navbar-collapse-target>div>ul>li").each(function(index){
			var text;
			
			if (width >= 768) {
					var link = $(this).find("a");
					
					link.css("color","rgb(218, 140, 58)");
					link.text(link.text().toUpperCase());
					text = $(this).html()
				
				$(this).css("color","rgb(218, 140, 58)");
			} else {
				text= originalText[index];
				$(this).css("color","rgb(53, 139, 78)");
			}
			
			$(this).html(text);
		});
	}
	
	var originalText = [];
	
	$("#navbar-collapse-target>div>ul>li").each(function(){
		originalText.push($(this).html());
	});
	
	$("#menu-toggle").on("click", function(e) {
		$("#menu-toggle").toggleClass("open");
	});
	
	$(window).on("resize",function(){
		menuItems($(window).width(), originalText);
	});
	
	menuItems($(window).width(), originalText);
});