<?php
namespace Sed\PrestationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dance
 *
 * @ORM\Table(name="dances")
 * @ORM\Entity(repositoryClass="Sed\PrestationsBundle\Repository\DanceRepository")
 */
class Dance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Length(min=3, minMessage="Le nom doit fait minimum {{ limit }} caractères.")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="duree", type="smallint", nullable=true)
     * @Assert\Range(min=1, minMessage="Une danse doit durer au moins une minute.")
     */
    private $duree = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="niveau", type="smallint")
     * @Assert\Range(min=1, minMessage="Un danse doit correspondre à un grade.")
     */
    private $niveau = 1;

    /**
     * @var bool
     *
     * @ORM\Column(name="hard", type="boolean")
     */
    private $hard = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="ceili", type="boolean")
     */
    private $ceili = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="final", type="boolean")
     */
    private $final = false;

    /**
     * @var int
     *
     * @ORM\Column(name="nbMinimum", type="smallint")
     * @Assert\Range(min=1, minMessage="Il faut au moins un danseur.")
     */
    private $nbMinimum = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="palierDanseurs", type="smallint")
     * @ORM\JoinColumn(nullable = true)
     */
    private $palierDanseurs;
    
    /**
     * @ORM\ManyToOne(targetEntity = "Sed\PrestationsBundle\Entity\Dance", inversedBy="variantes", cascade = {"persist"})
     * @ORM\JoinColumn(nullable = true)
     */
    private $variante;
    
    /**
     * @ORM\OneToMany(targetEntity = "Sed\PrestationsBundle\Entity\Dance", mappedBy="variante", cascade = {"remove"})
     */
    private $variantes;

    /**
     * @ORM\ManyToOne(targetEntity = "Sed\PrestationsBundle\Entity\Dance", inversedBy="sousParties", cascade= {"persist"})
     * @ORM\JoinColumn(nullable = true)
     */
    private $sousPartie;
    
    /**
     * @ORM\OneToMany(targetEntity="Sed\PrestationsBundle\Entity\Dance", mappedBy="sousPartie", cascade={"remove"})
     */
    private $sousParties;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="ratio", type="smallint", nullable=true)
     */
    private $ratio;

    /**
     * @var array
     */
    private $dancesList;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->variantes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sousParties = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return Dance
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set niveau
     *
     * @param integer $niveau
     *
     * @return Dance
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return integer
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set hard
     *
     * @param boolean $hard
     *
     * @return Dance
     */
    public function setHard($hard)
    {
        $this->hard = $hard;

        return $this;
    }

    /**
     * Get hard
     *
     * @return boolean
     */
    public function getHard()
    {
        return $this->hard;
    }

    /**
     * Set ceili
     *
     * @param boolean $ceili
     *
     * @return Dance
     */
    public function setCeili($ceili)
    {
        $this->ceili = $ceili;

        return $this;
    }

    /**
     * Get ceili
     *
     * @return boolean
     */
    public function getCeili()
    {
        return $this->ceili;
    }

    /**
     * Set final
     *
     * @param boolean $final
     *
     * @return Dance
     */
    public function setFinal($final)
    {
        $this->final = $final;

        return $this;
    }

    /**
     * Get final
     *
     * @return boolean
     */
    public function getFinal()
    {
        return $this->final;
    }

    /**
     * Set nbMinimum
     *
     * @param integer $nbMinimum
     *
     * @return Dance
     */
    public function setNbMinimum($nbMinimum)
    {
        $this->nbMinimum = $nbMinimum;

        return $this;
    }

    /**
     * Get nbMinimum
     *
     * @return integer
     */
    public function getNbMinimum()
    {
        return $this->nbMinimum;
    }

    /**
     * Set palierDanseurs
     *
     * @param integer $palierDanseurs
     *
     * @return Dance
     */
    public function setPalierDanseurs($palierDanseurs)
    {
        $this->palierDanseurs = $palierDanseurs;

        return $this;
    }

    /**
     * Get palierDanseurs
     *
     * @return integer
     */
    public function getPalierDanseurs()
    {
        return $this->palierDanseurs;
    }

    /**
     * Set ratio
     *
     * @param integer $ratio
     *
     * @return Dance
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;

        return $this;
    }

    /**
     * Get ratio
     *
     * @return integer
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * Set variante
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $variante
     *
     * @return Dance
     */
    public function setVariante(\Sed\PrestationsBundle\Entity\Dance $variante = null)
    {
        
        
        $this->variante = $variante;
        
        if ($variante !== null) {
            $variante->addVariante($this);
        }

        return $this;
    }

    /**
     * Get variante
     *
     * @return \Sed\PrestationsBundle\Entity\Dance
     */
    public function getVariante()
    {
        return $this->variante;
    }

    /**
     * Add variante
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $variante
     *
     * @return Dance
     */
    public function addVariante(\Sed\PrestationsBundle\Entity\Dance $variante)
    {
        $this->variantes[] = $variante;

        return $this;
    }

    /**
     * Remove variante
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $variante
     */
    public function removeVariante(\Sed\PrestationsBundle\Entity\Dance $variante)
    {
        $this->variantes->removeElement($variante);
    }

    /**
     * Get variantes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVariantes()
    {
        return $this->variantes;
    }

    /**
     * Set sousPartie
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $sousPartie
     *
     * @return Dance
     */
    public function setSousPartie(\Sed\PrestationsBundle\Entity\Dance $sousPartie = null)
    {
        $this->sousPartie = $sousPartie;
        
        if ($sousPartie !== null) {
            $sousPartie->addSousPartie($this);
        }

        return $this;
    }

    /**
     * Get sousPartie
     *
     * @return \Sed\PrestationsBundle\Entity\Dance
     */
    public function getSousPartie()
    {
        return $this->sousPartie;
    }

    /**
     * Add sousParty
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $sousParty
     *
     * @return Dance
     */
    public function addSousPartie(\Sed\PrestationsBundle\Entity\Dance $sousParty)
    {
        $this->sousParties[] = $sousParty;

        return $this;
    }

    /**
     * Remove sousParty
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $sousParty
     */
    public function removeSousPartie(\Sed\PrestationsBundle\Entity\Dance $sousParty)
    {
        $this->sousParties->removeElement($sousParty);
    }

    /**
     * Get sousParties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSousParties()
    {
        return $this->sousParties;
    }

    /**
     * Add sousParty
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $sousParty
     *
     * @return Dance
     */
    public function addSousParty(\Sed\PrestationsBundle\Entity\Dance $sousParty)
    {
        $this->sousParties[] = $sousParty;

        return $this;
    }

    /**
     * Remove sousParty
     *
     * @param \Sed\PrestationsBundle\Entity\Dance $sousParty
     */
    public function removeSousParty(\Sed\PrestationsBundle\Entity\Dance $sousParty)
    {
        $this->sousParties->removeElement($sousParty);
    }
    
    public function getDancesList()
    {
        return $this->dancesList;
    }
    
    public function setDancesList(array $dancesList)
    {
        $this->dancesList = $dancesList;
        
        return $this;
    }
}
