<?php

namespace Sed\PrestationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Number
 *
 * @ORM\Table(name="numbers")
 * @ORM\Entity(repositoryClass="Sed\PrestationsBundle\Repository\NumberRepository")
 */
class Number
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Sed\PrestationsBundle\Entity\Dance")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $dance;

    /**
     * @ORM\ManyToMany(targetEntity="Sed\PrestationsBundle\Entity\Dancer")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $dancers;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sed\PrestationsBundle\Entity\Prestation", inversedBy="danses")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $prestation;

    public function __construct()
    {
        $dancer = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dance
     *
     * @param Dance $dance
     *
     * @return Number
     */
    public function setDance(Dance $dance)
    {
        $this->dance = $dance;

        return $this;
    }

    /**
     * Get dance
     *
     * @return Dance
     */
    public function getDance()
    {
        return $this->dance;
    }

    /**
     * Add dancer
     *
     * @param Dancer $dancer
     *
     * @return Number
     */
    public function addDancer(Dancer $dancer)
    {
        $this->dancers[] = $dancer;

        return $this;
    }

    /**
     * Remove dancer
     *
     * @param Dancer $dancer
     */
    public function removeDancer(Dancer $dancer)
    {
        $this->dancers->removeElement($dancer);
    }

    /**
     * Get dancers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDancers()
    {
        return $this->dancers;
    }

    /**
     * Set prestation
     *
     * @param Prestation $prestation
     *
     * @return Number
     */
    public function setPrestation(Prestation $prestation)
    {
        $this->prestation = $prestation;
        $prestation->addDanse($this);

        return $this;
    }

    /**
     * Get prestation
     *
     * @return Prestation
     */
    public function getPrestation()
    {
        return $this->prestation;
    }
}
