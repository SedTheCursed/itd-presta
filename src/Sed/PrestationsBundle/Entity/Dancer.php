<?php

namespace Sed\PrestationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Dancer
 *
 * @ORM\Table(name="dancers")
 * @ORM\Entity(repositoryClass="Sed\PrestationsBundle\Repository\DancerRepository")
 * @UniqueEntity(fields="name", message="Un danseur du même nom existe déja.")
 * 
 */
class Dancer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     *  @Assert\Length(min=3, minMessage="Le nom doit faire {{ limit }} caractères minimum.")
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="sex", type="boolean")
     */
    private $sex = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="preference", type="boolean")
     */
    private $preference = false;

    /**
     * @var int
     *
     * @ORM\Column(name="niveau", type="smallint")
     * @Assert\Range(min=1, minMessage="Un danseur doit être, au minimum, grade un")
     */
    private $niveau;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email()
     */
    private $email;

    /**
      * @ORM\ManyToMany(targetEntity = "Sed\PrestationsBundle\Entity\Dance", cascade = {"persist"})
      * @ORM\JoinColumn(nullable = false)
      * @Assert\Valid()
     */
    private $dances;
    
    /**
     * @var array
     */
    private $dancesList;
    
    public function __construct()
    {
        $dances = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dancer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sex
     *
     * @param boolean $sex
     *
     * @return Dancer
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return bool
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set preference
     *
     * @param boolean $preference
     *
     * @return Dancer
     */
    public function setPreference($preference)
    {
        $this->preference = $preference;

        return $this;
    }

    /**
     * Get preference
     *
     * @return bool
     */
    public function getPreference()
    {
        return $this->preference;
    }

    /**
     * Set niveau
     *
     * @param integer $niveau
     *
     * @return Dancer
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return int
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Dancer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dances
     *
     * @param array $dances
     *
     * @return Dancer
     */
    public function setDances($dances)
    {
        $this->dances = $dances;

        return $this;
    }

    /**
     * Get dances
     *
     * @return array
     */
    public function getDances()
    {
        return $this->dances;
    }

    /**
     * Add dance
     *
     * @param Dance $dance
     *
     * @return Dancer
     */
    public function addDance(Dance $dance)
    {
        $this->dances[] = $dance;

        return $this;
    }

    /**
     * Remove dance
     *
     * @param Dance $dance
     */
    public function removeDance(Dance $dance)
    {
        $this->dances->removeElement($dance);
    }
    
    public function getDancesList()
    {
        return $this->dancesList;
    }

    public function setDancesList(array $dancesList)
    {
        $this->dancesList = $dancesList;
        
        return $this;
    }
}
