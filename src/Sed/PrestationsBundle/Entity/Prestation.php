<?php

namespace Sed\PrestationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Prestation
 *
 * @ORM\Table(name="prestations")
 * @ORM\Entity(repositoryClass="Sed\PrestationsBundle\Repository\PrestationRepository")
 */
class Prestation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255)
     * @Assert\Length(min=3, minMessage="Le lieu doit faire {{ limit }} caractères minimum")
     */
    private $lieu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Assert\Date()
     */
    private $date;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="duree", type="integer")
     * @Assert\Range(min=1, minMessage="Un prestation doit au moins faire une minute.")
     */
    private $duree=1;

    /**
     * @ORM\ManyToMany(targetEntity="Sed\PrestationsBundle\Entity\Dancer", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $danseurs;

    /**
      * @ORM\OneToMany(targetEntity="Sed\PrestationsBundle\Entity\Number", mappedBy="prestation", cascade={"remove"})
      * @Assert\Valid()
     */
    private $danses;

    public function __construct()
    {
        $this->danseurs = new ArrayCollection();
        $this->danses = new ArrayCollection();
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return Prestation
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Prestation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get danseurs
     *
     * @return array
     */
    public function getDanseurs()
    {
        return $this->danseurs;
    }

    /**
     * Get danses
     *
     * @return array
     */
    public function getDanses()
    {
        return $this->danses;
    }

    /**
     * Add danseur
     *
     * @param Dancer $danseur
     *
     * @return Prestation
     */
    public function addDanseur(Dancer $danseur)
    {
        $this->danseurs[] = $danseur;

        return $this;
    }

    /**
     * Remove danseur
     *
     * @param Dancer $danseur
     */
    public function removeDanseur(Dancer $danseur)
    {
        $this->danseurs->removeElement($danseur);
    }

    /**
     * Add danse
     *
     * @param Number $danse
     *
     * @return Prestation
     */
    public function addDanse(Number $danse)
    {
        $this->danses[] = $danse;
        
        return $this;
    }

    /**
     * Remove danse
     *
     * @param Number $danse
     */
    public function removeDanse(Number $danse)
    {
        $this->danses->removeElement($danse);
    }

    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return Prestation
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer
     */
    public function getDuree()
    {
        return $this->duree;
    }
}
