<?php
namespace Sed\PrestationsBundle\Planning;

use Sed\PrestationsBundle\Entity\Prestation;
use Sed\PrestationsBundle\Entity\Dance;

class Planning
{
    private $prestation;
    private $dancers;
    
    public function init(Prestation $prestation)
    {
        $this->prestation   = $prestation;
        $this->dancers      = $prestation->getDanseurs();
        
        $classment = new Classment($this->dancers);
        $classment->classDances();
        
        $schedule = new Schedule($this->prestation, $classment);
        $schedule->planning();
        
        $parameters = [
            "prestation"    => $this->prestation,
            "programme"     => $schedule->getPlanned(),
            "others"        => $schedule->getOthers(),
            "time"          => $schedule->getRemainingTime()
        ];
        
        return $parameters;
    }
}

