<?php
namespace Sed\PrestationsBundle\Planning;

class Classment
{
    private $dancers;
    private $faisable = [];
    private $possible = [];
    private $final;
    private $possibleFinals = [];
    
    public function __construct($dancers)
    {
        $this->dancers = $dancers;
    }
    
    public function classDances()
    {
        $dances = [];
        
        foreach ($this->dancers as $dancer) {
            foreach ($dancer->getDances() as $dance) {
                $nom = $dance->getName();
                
                if (array_key_exists($nom, $dances)) {
                    $dances[$nom]["dancers"][] = $dancer;
                } else {
                    $dances[$nom]=[
                        "dance" => $dance,
                        "dancers" => [$dancer]
                    ];
                }
            }
        }
        
        foreach($dances as $dance) {
            
            if ($dance["dance"]->getFinal()) {
                $this->final($dance);
            } else {
                $this->bodyDance($dance);
            }
        }
        
        foreach ($this->possible as $chore) {
            $dance = $chore["dance"];
            
            if (count($dance->getSousParties())>0) {
                unset($this->possible[$dance->getName()]);
                $this->danceWithSubParts($chore);
            }
        }
        
        foreach ($this->faisable as $chore) {
            $dance = $chore["dance"];
            
            if (count($dance->getSousParties())>0) {
                unset($this->faisable[$dance->getName()]);
                $this->danceWithSubParts($chore);
            }
        }
    }
    
    private function final(array $dance)
    {
        $nom = $dance["dance"]->getName();
        
        if (count($dance["dancers"]) === count($this->dancers) && empty($this->final)) {
            $this->final = $dance;
        } else {
            foreach ($this->dancers as $dancer) {
                if (!in_array($dancer, $dance["dancers"])) {
                    $dance["learners"][] = $dancer;
                }
            }
            $this->possibleFinals[$nom] = $dance;
        }
    }
    
    private function bodyDance(array $chore)
    {
        $dance      = $chore["dance"];
        $dancers    = $chore["dancers"];
        $nbMinimum  = $dance->getNbMinimum();
        $nbDanseurs = count($dancers);
        $nom        = $dance->getName();
        
        if ($nbDanseurs >= $nbMinimum) {
            if ($dance->getCeili() || $dance->getRatio() !== null) {
                $this->ceili($chore);
            } else {
                $this->faisable[$nom] = $chore;
            }
        } else {
            $chore["learners"] = [];
            
            foreach($this->dancers as $dancer) {
                if ($dancer->getNiveau() >= $dance->getNiveau() && !in_array($dancer, $dancers)) {
                    $chore["learners"][] = $dancer;
                }
            }
            
            $nbDanseurs = count($chore["dancers"]) + count($chore["learners"]);
            
            if ($nbDanseurs>$nbMinimum) {
                if ($dance->getCeili() || $dance->getRatio() !== null) {
                    $this->ceili($chore, false);
                } else {
                    $this->possible[$nom] = $chore;
                }
            }
        }
    }
    
    private function ceili(array $chore, bool $faisable = true)
    {
        $dance = $chore["dance"];
        $dancers = $chore["dancers"];
        $homme = $femme = $gent = $lady = [];
        $nom = $dance->getName();
        
        foreach($dancers as $dancer){
            ($dancer->getSex())? $homme[] = $dancer : $femme[] = $dancer;
            ($dancer->getPreference())? $gent[] = $dancer : $lady[] = $dancer;
        }
        
        if (!empty($chore["learners"])) {
            foreach($chore["learners"] as $dancer){
                ($dancer->getSex())? $homme[] = $dancer : $femme[] = $dancer;
                ($dancer->getPreference())? $gent[] = $dancer : $lady[] = $dancer;
            }
        }
        
        $ratio = ($dance->getRatio() !== null) ? 1/($dance->getRatio()+1) : .5;
        $quotaH = $dance->getNbMinimum()*$ratio;
        $quotaF = $dance->getNbMinimum()*(1-$ratio);
        
        if ((count($homme)>=$quotaH && count($femme)>= $quotaF) || (count($gent)>=$quotaH && count($lady) >= $quotaF)) {
            ($faisable)?$this->faisable[$nom] = $chore : $this->possible[$nom] = $chore;
        } else {
            ($faisable)?$this->possible[$nom] = $chore : "";
        }
    }
    
    private function danceWithSubParts (array $chore)
    {
        $dance = $chore["dance"];
        
        foreach($dance->getSousParties() as $sp) {
            $nom = $sp->getName();
            
            if (array_key_exists($nom, $this->faisable)) {
                $chore["sousPartie"][$nom] = $this->faisable[$nom];
                unset($this->faisable[$nom]);
                
                if (!isset($destination)) {
                    $destination = "faisable";
                }
            } elseif(array_key_exists($nom, $this->possible)) {
                $chore["sousPartie"][$nom] = $this->possible[$nom];
                unset($this->possible[$nom]);
                    
                if (!isset($destination) || $destination === "faisable") {
                    $destination = "possible";
                }
            } else {
                $destination = "impossible";
            }
        }
        
        $this->$destination[$dance->getName()]=$chore;
    }
    
    /**
     * GETTERS
     */
    public function getFaisable()
    {
        return $this->faisable;
    }

    public function getPossible()
    {
        return $this->possible;
    }
    public function getFinal()
    {
        return $this->final;
    }

    public function getPossibleFinals()
    {
        return $this->possibleFinals;
    }
}

