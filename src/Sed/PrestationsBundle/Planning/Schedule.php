<?php
namespace Sed\PrestationsBundle\Planning;

use Sed\PrestationsBundle\Entity\Prestation;
use Sed\PrestationsBundle\Entity\Dance;

class Schedule
{
    private $prestation;
    private $remainingTime;
    private $classment;
    private $faisable;
    private $possible;
    private $planned;
    private $others;
    private $repos = [];
    
    public function __construct(Prestation $prestation, Classment $classment)
    {
        $this->prestation       = $prestation;
        $this->remainingTime    = $prestation->getDuree();
        $this->classment        = $classment;
        $this->faisable         = $classment->getFaisable();
        $this->possible         = $classment->getPossible();
    }
    
    public function planning()
    {
        $this->planned["chore"] = [];
        
        $this->setFinal();
        $this->setPenultimate();
        
        while ($this->remainingTime > 0) {
            $this->setDance();
        }
        
        foreach ($this->faisable as $chore) {
            $this->others["chore"][] = $chore; 
        }
        
        foreach ($this->possible as $chore) {
            $this->others["chore"][] = $chore;
        }
    }
        
    private function setDance(string $type = "first")
    {
        $duree  = 0;
        $grade  = 1;
        $danceArray = ($type === "first" || $type === "withoutRest")?$this->faisable:$this->possible;
        $newDance;

        foreach ($danceArray as $chore) {
            $dance      = $chore["dance"];
            $dancers    = $chore["dancers"];
            
            if (isset($chore["learners"])) {
                foreach ($chore["learners"] as $dancer) {
                    $dancers[] = $dancer;
                }
            }
            
            $danceDuree = $dance->getDuree();
            $danceGrade = $dance->getNiveau();
            
            if ($this->remainingTime - $danceDuree < 0) { //on verifie s'il y a assez de temps pour faire la danse
                continue;
            }
            
            //Verification que la danse est faisable sans les danseurs de la danse suivante
            if ($type != "withoutRest" && $type != "possibleWithoutRest") {
                $restingDancers = 0;
                foreach ($this->repos as $dancer) {
                    (in_array($dancer, $dancers))?$restingDancers++:"";
                }
                
                $availableDancers = count($dancers)-$restingDancers;
                
                if ($availableDancers < $dance->getNbMinimum()) {
                    continue;
                }
            }
            
            if($danceGrade >= $grade && $danceDuree >= $duree) {
                $newDance   = $chore;
                $grade      = $danceGrade;
                $duree      = $danceDuree;
            }
        }
        
        if (!empty($newDance)) {
            $this->addDance($newDance, $danceArray);
        } else {
            switch($type) {
                case "first" :
                    $this->setDance("withoutRest");
                    break;
                case "withoutRest" :
                    $this->setDance("possible");
                    break;
                case "possible":
                    $this->setDance("possibleWithoutRest");
                    break;
                default:
                    $this->remainingTime = 0;
            }           
        }
    }
    
    private function setPenultimate()
    {
        if (!empty($this->planned["final"])) {
            $shoes = $this->planned["final"]["dance"]->getHard();
        } else {
            $shoes = true;
        }
        
        $grade      = 1;
        $nbDanseurs = 100;
        $penultimate;
        
        foreach ($this->faisable as $chore) {
            $dance = $chore["dance"];
            $danceShoes = $dance->getHard();
            $danceGrade = $dance->getNiveau();
            $danceDanseurs = $dance->getNbMinimum();
            
            if($danceShoes === $shoes && $danceGrade >= $grade && $danceDanseurs < $nbDanseurs) {
                $penultimate    = $chore;
                $grade          = $danceGrade;
                $nbDanseurs     = $danceDanseurs;
            }
        }
        
        if(!empty($penultimate)) {
            $this->addDance($penultimate, $this->faisable);
        }
    }
    
    private function setFinal()
    {
        $final = $this->classment->getFinal();
        
        if ($final) {
            $this->planned["final"] = $final;
            $this->remainingTime -= $final["dance"]->getDuree();
        }
        
        foreach ($this->classment->getPossibleFinals() as $other) {
            $this->others["finals"][] = $other;
        }
    }
    
    private function addDance(array $chore, array $array)
    {
        $dance      = $chore["dance"];
        $dancers    = $chore["dancers"];
        $learners   = (isset($chore["learners"]))?$chore["learners"]:[];
        
        if ($array === $this->faisable) {            
            unset($this->faisable[$dance->getName()]);
        } else {
            unset($this->possible[$dance->getName()]);
        }
        
        //Passage des variantes de la danses dans les danses possibles
        $variantes = [];
        
        if (!empty($dance->getVariante())) {
            $variantes[] = $dance->getVariante();
            
            foreach ($dance->getVariante()->getVariantes() as $variante) {
                $variantes[] = $variante;
            }
        } elseif (!empty($dance->getVariantes())) {
            foreach ($dance->getVariantes() as $variante) {
                $variantes[] = $variante;
            }
        }
        
        foreach ($variantes as $variante) {
            if (array_key_exists($variante->getName(), $this->faisable)) {
                $set = $this->faisable[$variante->getName()];
                unset($this->faisable[$variante->getName()]);
                $this->others["chore"][] = $set;
            }
            
            if (array_key_exists($variante->getName(), $this->possible)) {
                $set = $this->possible[$variante->getName()];
                unset($this->possible[$variante->getName()]);
                $this->others["chore"][] = $set;
            }
        }
        
        $this->remainingTime -= $dance->getDuree();
        
        //Affectation des danseurs
        $dancers = $this->setActifs($dancers, $dance, $learners);

        $this->repos        = $dancers["actifs"];
        $chore["actifs"]    = $dancers["actifs"];
        $chore["reserve"]   = $dancers["reserve"];
        unset($chore["dancers"]);
        
        array_unshift($this->planned["chore"], $chore);
    }
    
    private function setActifs(array $dancers, Dance $dance, array $learners) {
        $nbDanseurs = $dance->getNbMinimum();
        $palier     = $dance->getPalierDanseurs();
        $niveau     = $dance->getNiveau();
        $actifs     = [];
        $reserve    = [
            "dancers"   => [],
            "learners"  => $learners
        ];
        
        if ($palier > 0) {
            $nbDancers  = count($dancers);
            $nbTotal    = count($this->prestation->getDanseurs())/2;
            
            while ($nbDanseurs+$palier < $nbDancers && $nbDanseurs+$palier < $nbTotal) {
                $nbDanseurs += $palier;
            }
        }
        
        if ($dance->getCeili() || $dance->getRatio() !== null) {
            $ratio  = ($dance->getRatio() !== null) ? 1/($dance->getRatio()+1) : .5;
            $quotaH = $nbDanseurs*$ratio;
            $quotaF = $nbDanseurs*(1-$ratio);
            $gents  = 0;
            $lady   = 0;
        }
        
        foreach ($dancers as $dancer) {
            $nbActifs = count($actifs);
            
            if (in_array($dancer, $this->repos)) {
                $reserve["dancers"][] = $dancer;
                continue;
            }
            
            if (isset($ratio)) {
                $preference = ($dancer->getPreference())?"gent":"lady";
                
                if ($preference === "gent" && $gents < $quotaH) {
                    $gents++;
                    $horsQuota = false;
                } elseif ($preference === "lady" && $lady < $quotaF) {
                    $lady++;
                    $horsQuota = false;
                } else {
                    $horsQuota = true;
                }
            }
            
            if ($nbActifs === $nbDanseurs || (isset($horsQuota) && $horsQuota)) {
                $ecartNiveauDancer  = abs($niveau-$dancer->getNiveau());
                $ecartMax           = 0;
                
                for ($i=0; $i<$nbActifs; $i++) {
                    $ecartNiveauActif = abs($niveau-$actifs[$i]->getNiveau());
                    $sameGender = ($actifs[$i]->getPreference() === $dancer->getPreference());
                    
                    if ($ecartNiveauDancer < $ecartNiveauActif
                    && $ecartNiveauActif > $ecartMax
                    && $sameGender) {
                        $ecartMax   = $ecartNiveauActif;
                        $index      = $i;
                    }
                }
                
                if (isset($index)) {
                    $reserve["dancers"][]      = $actifs[$index];
                    $actifs[$index] = $dancer;
                } else {
                    $reserve["dancers"][]      = $dancer;
                }
            } else {
                $actifs[] = $dancer;
            }
        }
        
        $classedDancers["actifs"]    = $actifs;
        $classedDancers["reserve"]   = $reserve;
        
        return $classedDancers;
    }
    
    /**
     * GETTERS
     */
    
    public function getPrestation()
    {
        return $this->prestation;
    }
    
    public function getRemainingTime()
    {
        return $this->remainingTime;
    }
    public function getPlanned()
    {
        return $this->planned;
    }

    public function getOthers()
    {
        return $this->others;
    }
}

