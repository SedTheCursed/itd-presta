<?php

namespace Sed\PrestationsBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * DancerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DancerRepository extends \Doctrine\ORM\EntityRepository
{    
    public function getDancers(int $page, int $nbPerPage)
    {
        $query = $this->createQueryBuilder("d")
            ->orderBy("d.name")
            ->setFirstResult(($page-1)*$nbPerPage)
            ->setMaxResults($nbPerPage)
        ;
        
        return new Paginator($query);
    }
    
    public function getDancer(int $id)
    {
        $qb = $this->createQueryBuilder("d")
            ->where("d.id = :id")
            ->setParameter("id", $id)
            ->leftJoin("d.dances","d2")
            ->addSelect("d2");
        
        return $qb->getQuery()->getOneOrNullResult();        
    }
    
    public function dancersForNumbers()
    {
        foreach ($this->findAll() as $dancer) {
            $dancers[$dancer->getId()] = $dancer;
        }
        
        return $dancers;
    }
}
