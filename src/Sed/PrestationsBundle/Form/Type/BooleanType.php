<?php
namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class BooleanType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {        
        $resolver->setDefaults([
            "multiple"  => false,
            "expanded"  => true,
            "choices"   => [
                "Oui"   => true,
                "Non"   => false
            ]
        ]);
    }
    
    public function getParent()
    {
        return ChoiceType::class;
    }
}

