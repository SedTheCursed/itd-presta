<?php
namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DancesCheckboxType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {        
        
        $resolver->setDefaults([
            "class"         => "SedPrestationsBundle:Dance",
            "choice_label"  => "name",
            "required"      => true,
            "choice_value"  => "id",
            "multiple"      => true,
            "expanded"      => true
        ]);
    }
    
    public function getParent()
    {
        return EntityType::class;
    }
}

