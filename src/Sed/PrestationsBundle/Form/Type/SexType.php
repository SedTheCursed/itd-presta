<?php
namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SexType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {        
        $resolver->setDefaults([
            "multiple"  => false,
            "expanded"  => true,
            "choices"   => [
                "Homme"   => true,
                "Femme"   => false
            ]
        ]);
    }
    
    public function getParent()
    {
        return ChoiceType::class;
    }
}

