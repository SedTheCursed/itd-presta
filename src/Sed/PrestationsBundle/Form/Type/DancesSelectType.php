<?php
namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DancesSelectType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {        
        
        $resolver->setDefaults([
            "choice_label"  => "name",
            "placeholder"   => "Aucune",
            "required"      => false,
            "choice_value"  => "id"
        ]);
    }
    
    public function getParent()
    {
        return ChoiceType::class;
    }
}

