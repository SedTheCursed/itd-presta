<?php

namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class DanceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',           TextType::class)
            ->add('duree',          IntegerType::class, ["required" => false])
            ->add('niveau',         GradesType::class)
            ->add('hard',           BooleanType::class)
            ->add('ceili',          BooleanType::class)
            ->add('final',          BooleanType::class)
            ->add('nbMinimum',      IntegerType::class)
            ->add('palierDanseurs', IntegerType::class)
            ->add('ratio',          IntegerType::class, ["required" => false])
            ->add('ajouter',        SubmitType::class)
        ;
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(formEvent $event) {
                if (is_null($event->getData())) {
                    return;
                }
                
                $dances = $event->getData()->getDancesList();
                
                $event->getForm()
                    ->add("variante",   DancesSelectType::class, [ "choices" => $dances, "required" => false ])
                    ->add("sousPartie", DancesSelectType::class, [ "choices" => $dances, "required" => false ])
                ;
            }
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {   
        $resolver->setDefaults(array(
            'data_class' => 'Sed\PrestationsBundle\Entity\Dance',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sed_prestationsbundle_dance';
    }


}
