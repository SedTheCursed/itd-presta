<?php
namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GradesType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        for($i=1;$i<=12;$i++) {
            $grades[$i]=$i;
        }
        
        $resolver->setDefaults([
            "choices" => $grades
        ]);
    }
    
    public function getParent()
    {
        return ChoiceType::class;
    }
}

