<?php

namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sed\PrestationsBundle\Repository\DancerRepository;

class PrestationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lieu',       TextType::class)
            ->add('date',       DateType::class)
            ->add('duree',      IntegerType::class)
            ->add('danseurs',   EntityType::class, [
                "class"         =>  "SedPrestationsBundle:Dancer",
                "choice_label"  =>  "name",
                "multiple"      =>  true,
                "expanded"      =>  true,
                "query_builder" => function (DancerRepository $er) {
                    return $er
                        ->createQueryBuilder('d')
                        ->orderBy("d.name");
                }
            ])
            ->add("ajouter",    SubmitType::class)
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sed\PrestationsBundle\Entity\Prestation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sed_prestationsbundle_prestation';
    }


}
