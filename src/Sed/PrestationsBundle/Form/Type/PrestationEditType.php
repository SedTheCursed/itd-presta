<?php
namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PrestationEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("modifier",    SubmitType::class)
            ->remove("ajouter")
        ;
    }
    
    public function getParent()
    {
        return PrestationType::class;
    }
}

