<?php

namespace Sed\PrestationsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class DancerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',       TextType::class)
            ->add('sex',        SexType::class)
            ->add('preference', SexType::class)
            ->add('niveau',     GradesType::class)
            ->add('email',      EmailType::class)
            ->add('ajouter',    SubmitType::class)
        ;
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(formEvent $event) {
                if (is_null($event->getData())) {
                    return;
                }
                
                $dances = $event->getData()->getDancesList();
                
                $event->getForm()
                    ->add("dances",   DancesCheckboxType::class, [ "choices" => $dances ])
                ;
            }
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sed\PrestationsBundle\Entity\Dancer'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sed_prestationsbundle_dancer';
    }


}
