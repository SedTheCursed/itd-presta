<?php
namespace Sed\PrestationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sed\PrestationsBundle\Entity\Dance;
use Sed\PrestationsBundle\Form\Type\DanceType;
use Sed\PrestationsBundle\Form\Type\DanceEditType;
use Symfony\Component\HttpFoundation\Request;

class DancesController extends Controller
{
    public function addAction(Request $request)
    {   
        $dance = new Dance();
        $dance->setDancesList($this->getDoctrine()->getManager()->getRepository("SedPrestationsBundle:Dance")->getList());
        $form = $this->createForm(DanceType::class, $dance);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dance);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Danse bien enregistrée.");
            
            return $this->redirectToRoute("sed_prestations_dances_show");
        }
        
        return $this->render("SedPrestationsBundle:Dances:add.html.twig", ["form"   => $form->createView()]);
    }
    
    public function showAction(int $page)
    {   
        $nbPerPage = $this->container->getParameter("items_per_page");
        $repository = $this->getDoctrine()->getManager()->getRepository("SedPrestationsBundle:Dance");
        $dances = $repository->getDances($page, $nbPerPage);
        
        if ($page > $dances["nbPages"] && $page !== 1) { // Si la page n'existe pas, on retourne une 404
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }
        
        $parameters = [
            "dances"    => $dances["dances"],
            "page"      => $page,
            "nbPages"   => $dances["nbPages"]
        ];
        
        return $this->render("SedPrestationsBundle:Dances:show.html.twig", $parameters);
    }
        
    public function updateAction(Dance $dance, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $dance->setDancesList($this->getDoctrine()->getManager()->getRepository("SedPrestationsBundle:Dance")->getList());
        $form = $this->createForm(DanceEditType::class, $dance);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Danse bien modifiée.");
        }
        
        $parameters = [
            "form" => $form->createView(),
            "dance" => $dance
        ];
        
        return $this->render("SedPrestationsBundle:Dances:update.html.twig", $parameters);
    }
    
    public function deleteAction(Dance $dance, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->get("form.factory")->create();
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em->remove($dance);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Danse bien supprimée.");
            
            return $this->redirectToRoute("sed_prestations_dances_show");
        }
        
        $parameters = [
            "form"     => $form->createView(),
            "dance"    => $dance
        ];
        
        return $this->render('SedPrestationsBundle:Dances:delete.html.twig', $parameters);
    }
}
