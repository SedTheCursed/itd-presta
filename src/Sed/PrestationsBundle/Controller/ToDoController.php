<?php
namespace Sed\PrestationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ToDoController extends Controller
{    
    /**
     * @Security("has_role('ROLE_DEV')")
     */
    public function showAction()
    {
        return $this->render("SedPrestationsBundle:ToDO:show.html.twig");
    }
}

