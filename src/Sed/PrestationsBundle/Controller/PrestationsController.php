<?php
namespace Sed\PrestationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sed\PrestationsBundle\Entity\Number;
use Sed\PrestationsBundle\Entity\Prestation;
use Sed\PrestationsBundle\Form\Type\PrestationType;
use Sed\PrestationsBundle\Form\Type\PrestationEditType;
use Symfony\Component\HttpFoundation\Request;

class PrestationsController extends Controller
{    
    public function addAction(Request $request)
    {
        $prestation = new Prestation();
        $form = $this->createForm(PrestationType::class, $prestation);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($prestation);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Prestation bien enregistrée.");
            
            return $this->redirectToRoute("sed_prestations_prestations_programme", ["id" => $prestation->getId()]);
        }
        
        return $this->render("SedPrestationsBundle:Prestations:add.html.twig", ["form" => $form->createView()]);
    }
    
    public function indexAction(int $page)
    {        
        $nbPerPage = $this->container->getParameter("items_per_page");
        $prestations = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository("SedPrestationsBundle:Prestation")
            ->getPrestations($page, $nbPerPage)
        ;
        
        $nbPages = ceil(count($prestations["anciennes"])/$nbPerPage);
        
        // Si la page n'existe pas, on retourne une 404
        if ($page > $nbPages && $page !== 1) {    
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");    
        }
        
        $parameters = [
            "prestations"   => $prestations["prestations"],
            "anciennes"     => $prestations["anciennes"],
            "nbPages"       => $nbPages,
            "page"          => $page
        ];
        
        return $this->render("SedPrestationsBundle:Prestations:index.html.twig", $parameters);
    }
    
    public function updateAction(int $id, Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
        
        $prestation = $em->getRepository("SedPrestationsBundle:Prestation")->getPrestation($id);
        
        if ($prestation === null) {
            throw new NotFoundHttpException("La prestation n'".$id." n'existe pas.");
        }
        
        $form = $this->createForm(PrestationEditType::class, $prestation);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Prestation bien modifiée.");
            
            return $this->redirectToRoute("sed_prestations_prestations_programme", ["id" => $prestation->getId()]);
        }
        
        $parameters = [
            "prestation"    => $prestation,
            "form"          => $form->createView()
        ];
        
        return $this->render("SedPrestationsBundle:Prestations:update.html.twig", $parameters);
    }
    
    public function programmeAction(int $id, Request $request)
    {
        $manager = $this
            ->getDoctrine()
            ->getManager();
        $prestation = $manager
            ->getRepository("SedPrestationsBundle:Prestation")
            ->getFullPrestation($id);
        
        if ($prestation === null) {
            throw new NotFoundHttpException("La prestation n'".$id." n'existe pas.");  
        }
        
        $planning = $this->get("sed_prestations_planning");
        
        if ($request->isMethod("POST") && $request->request->get("programme") !== null) {
            $programme  = $request->request->get("programme");
            
            $danses     = $manager->getRepository("SedPrestationsBundle:Dance")->dancesForNumbers();
            $danseurs   = $manager->getRepository("SedPrestationsBundle:Dancer")->dancersForNumbers();
            
           if(!empty($prestation->getDanses())){
               foreach ($prestation->getDanses() as $former) {
                   $manager->remove($former);
               }
           }
            
            foreach ($programme as $number) {
                $chore  = new Number();
                $danse  = $danses[$number["dance"]];
            
                $chore->setPrestation($prestation);
                $chore->setDance($danse);
                
                foreach ($number["dancers"] as $dancerId) {
                    $danseur = $danseurs[$dancerId];
                    
                    $chore->addDancer($danseur);
                }
                
                $manager->persist($chore);
            }
            
            $manager->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Programme bien mis à jour.");
            
        }
        
        $parameters = $planning->init($prestation);
        
        return $this->render("SedPrestationsBundle:Prestations:programme.html.twig", $parameters);
    }
    
    public function deleteAction(Prestation $prestation, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->get("form.factory")->create();
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em->remove($prestation);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Prestation bien supprimée.");
            
            return $this->redirectToRoute("sed_prestations_back_homepage");
        }
        
        $parameters = [
            "form"          => $form->createView(),
            "prestation"    => $prestation
        ];
            
        return $this->render('SedPrestationsBundle:Prestations:delete.html.twig', $parameters);
    }
}
