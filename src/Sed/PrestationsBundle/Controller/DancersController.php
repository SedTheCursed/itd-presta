<?php
namespace Sed\PrestationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sed\PrestationsBundle\Entity\Dancer;
use Sed\PrestationsBundle\Form\Type\DancerType;
use Sed\PrestationsBundle\Form\Type\DancerEditType;

class DancersController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dancer = new Dancer();
        $dancer->setDancesList($em->getRepository("SedPrestationsBundle:Dance")->getList());
        $form = $this->createForm(DancerType::class, $dancer);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em->persist($dancer);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Danseur bien enregistré.");
            
            return $this->redirectToRoute("sed_prestations_dancers_show");
        }
        
        $parameters = [
            "form"  => $form->createView() 
        ];
        
        return $this->render("SedPrestationsBundle:Dancers:add.html.twig", $parameters);
    }
    
    public function showAction(int $page)
    {   
        $nbPerPage = $this->container->getParameter("items_per_page");
        $dancers = $this->getDoctrine()->getManager()->getRepository("SedPrestationsBundle:Dancer")->getDancers($page, $nbPerPage);
        $nbPages = ceil(count($dancers)/$nbPerPage);
        
        // Si la page n'existe pas, on retourne une 404
        if ($page > $nbPages && $page !== 1) {
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }
        
        $parameters = [
            "dancers"   => $dancers,
            "page"      => $page,
            "nbPages"   => $nbPages
        ];
        
        return $this->render("SedPrestationsBundle:Dancers:show.html.twig", $parameters);
    }
    
    public function updateAction(Dancer $dancer, Request $request)
    {        
        $em = $this->getDoctrine()->getManager();
        
        $dancer->setDancesList($em->getRepository("SedPrestationsBundle:Dance")->getList());
        $form = $this->createForm(DancerEditType::class, $dancer);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Danseur bien modifié.");
        }
        
        $parameters = [
            "dancer"    => $dancer,
            "form"      => $form->createView()
        ];
        
        return $this->render("SedPrestationsBundle:Dancers:update.html.twig", $parameters);
    }
    
    public function deleteAction(Dancer $dancer, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->get("form.factory")->create();
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em->remove($dancer);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Danseur bien supprimé.");
            
            return $this->redirectToRoute("sed_prestations_dancers_show");
        }
        
        $parameters = [
            "form"      => $form->createView(),
            "dancer"    => $dancer
        ];
        
        return $this->render('SedPrestationsBundle:Dancers:delete.html.twig', $parameters);
    }
}
