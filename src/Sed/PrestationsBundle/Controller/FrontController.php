<?php
namespace Sed\PrestationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sed\PrestationsBundle\Entity\Prestation;

class FrontController extends Controller
{
    public function indexAction()
    {
        return $this->render("SedPrestationsBundle:Front:index.html.twig");
    }
    
    public function viewAction(int $id)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository("SedPrestationsBundle:Prestation");
        $prestation = $repository->getPrestation($id);
        
        if ($prestation === null) {
            throw new NotFoundHttpException("La prestation n'".$id." n'existe pas.");
        }
            
        $entourage = $repository->getEntourage($prestation->getId());

        $parameters = ([
            "prestation"    => $prestation,
            "entourage"     => $entourage
        ]);
        
        return $this->render("SedPrestationsBundle:Front:view.html.twig", $parameters);
    }
    
    public function incomingAction()
    {
        $prestations = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository("SedPrestationsBundle:Prestation")
        ->getIncoming($this->getParameter("items_per_page"))
        ;
        
        $parameters = ["prestations" => $prestations];
        
        return $this->render("SedPrestationsBundle:Front:incoming.html.twig", $parameters);
    }
}
