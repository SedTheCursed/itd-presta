<?php

namespace Sed\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\UserBundle\Form\Type\ProfileFormType;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles', ChoiceType::class, [
                "choices"   => [
                    "Utilisateur"       => "ROLE_USER",
                    "Administrateur"    => "ROLE_ADMIN",
                    "Développeur"       => "ROLE_DEV"
                    
                ],
                'multiple'  => true,
                "expanded"  => true
            ])
            ->add("modifier",   SubmitType::class)
            
        ;
    }

    public function getParent()
    {
        return ProfileFormType::class;
    }
}
