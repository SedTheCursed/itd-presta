<?php

namespace Sed\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use FOS\UserBundle\Form\Type\RegistrationFormType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles', ChoiceType::class, [
                "choices"   => [
                    "Utilisateur"       => "ROLE_USER",
                    "Administrateur"    => "ROLE_ADMIN",
                    "Développeur"       => "ROLE_DEV"
                    
                ],
                'multiple'  => true,
                "expanded"  => true
            ])
            ->add('ajouter',    SubmitType::class)
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sed\UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sed_userbundle_user';
    }

    public function getParent()
    {
        return RegistrationFormType::class;
    }
}
