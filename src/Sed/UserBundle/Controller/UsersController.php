<?php
namespace Sed\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Sed\UserBundle\Entity\User;
use Sed\UserBundle\Form\Type\UserType;
use Sed\UserBundle\Form\Type\UserEditType;
use Sed\UserBundle\Form\Type\ChangePasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UsersController extends Controller
{    
    
    /**
     * @Security("has_role('ROLE_DEV')")
     */
    public function addAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);
            $em->persist($user);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Utilisateur bien enregistré.");
            
            return $this->redirectToRoute("sed_users_show");
        }
        
        return $this->render("SedUserBundle:User:add.html.twig", ["form" => $form->createView()]);
    }
    
    
    /**
     * @Security("has_role('ROLE_DEV')")
     */
    public function showAction(int $page)
    {        
        $nbPerPage = $this->container->getParameter("items_per_page");
        $users = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository("SedUserBundle:User")
            ->getUsers($page, $nbPerPage)
        ;
        
        $nbPages = ceil(count($users)/$nbPerPage);
        
        // Si la page n'existe pas, on retourne une 404
        if ($page > $nbPages && $page !== 1) {    
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");    
        }
        
        $parameters = [
            "users"     => $users,
            "nbPages"   => $nbPages,
            "page"      => $page
        ];
        
        return $this->render("SedUserBundle:User:show.html.twig", $parameters);
    }
      
    /**
     * @Security("has_role('ROLE_DEV')")
     */
    public function updateAction(User $user, Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(UserEditType::class, $user);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $user->setEnabled(true);
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Utilisateur bien modifié.");
        }
        
        $parameters = [
            "user"  => $user,
            "form"  => $form->createView()
        ];
        
        return $this->render("SedUserBundle:User:update.html.twig", $parameters);
    }
    
    /**
     * @Security("has_role('ROLE_DEV')")
     */
     public function changePasswordAction(User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ChangePasswordType::class, $user);
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $password = password_hash($user->getPlainPassword(), PASSWORD_BCRYPT);
            $user->setPassword($password);
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Utilisateur bien modifié.");
        }
        
        return $this->render('@FOSUser/ChangePassword/change_password.html.twig', array(
            'form'  => $form->createView(),
            'user'  => $user
        ));
    }
    
    /**
     * @Security("has_role('ROLE_DEV')")
     */
    public function deleteAction(User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->get("form.factory")->create();
        
        if ($request->isMethod("POST") && $form->handleRequest($request)->isValid()) {
            $em->remove($user);
            
            $em->flush();
            
            $request->getSession()->getFlashBag()->add("notice","Utilisateur bien supprimé.");
            
            return $this->redirectToRoute("sed_users_show");
        }
        
        $parameters = [
            "form"  => $form->createView(),
            "user"  => $user
        ];
            
        return $this->render('SedUserBundle:User:delete.html.twig', $parameters);
    }
}
