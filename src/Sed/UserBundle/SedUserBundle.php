<?php

namespace Sed\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SedUserBundle extends Bundle
{
    public function getParent()
    {
        return "FOSUserBundle";
    }
}
